//motor A connected between A01 and A02
//motor B connected between B01 and B02

int STBY = 20; //standby

//Motor A
int PWMA = 21; //Speed control


//Motor B
int PWMB = 22; //Speed control


//Other Motor A

int PWMAI = 23; //Speed control

// defines pins numbers for stepper
const int stepPin = 2; 
const int dirPin = 3;

void setup() {
  pinMode(STBY, OUTPUT);
  pinMode(PWMA, OUTPUT);
  pinMode(PWMB, OUTPUT);
  pinMode(PWMAI, OUTPUT);
  // Sets the two pins as Outputs for stepper
  pinMode(stepPin,OUTPUT); 
  pinMode(dirPin,OUTPUT);
}

void loop() {
  move(1, 255, 1); //motor 1, full speed, left
  move(2, 255, 1); //motor 2, full speed, left

  delay(1000); //go for 1 second
  stop(); //stop
  delay(250); //hold for 250ms until move again

  move(1, 128, 0); //motor 1, half speed, right
  move(2, 128, 0); //motor 2, half speed, right
  move(3, 128, 0); //motor 3, half speed, right

  delay(1000);
  stop();
  delay(250);
}


void move(int motor, int speed, int direction) {
  //Move specific motor at speed and direction
  //motor: 0 for B 1 for A
  //speed: 0 is off, and 255 is full speed
  //direction: 0 clockwise, 1 counter-clockwise

  digitalWrite(STBY, HIGH); //disable standby

  boolean inPin1 = LOW;
  boolean inPin2 = HIGH;

  if (direction == 1) {
    inPin1 = HIGH;
    inPin2 = LOW;
  }

  if (motor == 1) {
    //   digitalWrite(AIN1, inPin1);
    //   digitalWrite(AIN2, inPin2);
    analogWrite(PWMA, speed);
  }
  if (motor == 2) {
    //   digitalWrite(BIN1, inPin1);
    //   digitalWrite(BIN2, inPin2);
    analogWrite(PWMB, speed);
  }
  else {
    //   digitalWrite(BIN1, inPin1);
    //   digitalWrite(BIN2, inPin2);
    analogWrite(PWMAI, speed);
  }
}

void stop() {
  //enable standby
  digitalWrite(STBY, LOW);
  digitalWrite(dirPin,HIGH); // Enables the motor to move in a particular direction
  // Makes 200 pulses for making one full cycle rotation
  for(int x = 0; x < 800; x++) {
    digitalWrite(stepPin,HIGH); 
    delayMicroseconds(700); 
    digitalWrite(stepPin,LOW); 
    delayMicroseconds(700); 
  }
  delay(1000); // One second delay
  
  digitalWrite(dirPin,LOW); //Changes the rotations direction
  // Makes 400 pulses for making two full cycle rotation
  for(int x = 0; x < 800; x++) {
    digitalWrite(stepPin,HIGH);
    delayMicroseconds(700);
    digitalWrite(stepPin,LOW);
    delayMicroseconds(700);
  }
  delay(1000);


  
}
